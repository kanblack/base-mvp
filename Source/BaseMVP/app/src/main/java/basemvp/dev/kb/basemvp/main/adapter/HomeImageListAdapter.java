package basemvp.dev.kb.basemvp.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Priority;
import com.paditech.base.common.BaseRecycleViewAdapter;
import com.paditech.base.image.GlideApp;

import java.util.ArrayList;
import java.util.List;

import basemvp.dev.kb.basemvp.R;
import butterknife.BindView;

public class HomeImageListAdapter extends BaseRecycleViewAdapter {
    List<String> imageSource = new ArrayList<>();

    @Override
    public String getItem(int position) {
        return imageSource.get(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false));
    }

    @Override
    public int getItemCount() {
        return imageSource.size();
    }

    public void replace(List<String> data) {
        if (data == null) {
            return;
        }
        imageSource.clear();
        imageSource.addAll(data);
        notifyDataSetChanged();
    }

    class ImageHolder extends BaseRecycleViewAdapter.BaseViewHolder {
        @BindView(R.id.imvThumb)
        ImageView imvThumb;

        ImageHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onBindingData(int position) {
            String url = getItem(position);
            if (url != null) {
                GlideApp.with(itemView).load(url).thumbnail(0.5f).priority(Priority.IMMEDIATE).centerCrop().into(imvThumb);
            } else {
                imvThumb.setImageDrawable(null);
            }
        }
    }
}
