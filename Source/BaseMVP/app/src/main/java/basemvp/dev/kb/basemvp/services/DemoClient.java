package basemvp.dev.kb.basemvp.services;

import com.paditech.base.network.BaseClient;

public class DemoClient extends BaseClient<DemoService> {
    public static DemoClient newInstance() {
        return new DemoClient();
    }

    @Override
    protected Class<DemoService> getServiceType() {
        return DemoService.class;
    }

    @Override
    protected String getHostAddress() {
        return "https://demo0858711.mockable.io/";
    }
}
