package basemvp.dev.kb.basemvp.main.contracts;

import com.paditech.base.mvp.BasePresenterImpl;
import com.paditech.base.mvp.BaseViewImpl;

import java.util.List;

public interface HomeContract {
    interface ViewImpl extends BaseViewImpl {
        void setText(String text);

        void setBackGroundColor(int color);

        void fillImage(List<String> data);
    }

    interface Presenter extends BasePresenterImpl {
        void updateBackGround();

        void updateText();

        void loadImages();
    }
}
