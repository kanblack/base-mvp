package basemvp.dev.kb.basemvp.main.views.activities;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.paditech.base.helper.ViewHelper;
import com.paditech.base.mvp.MVPActivity;

import java.util.List;

import basemvp.dev.kb.basemvp.R;
import basemvp.dev.kb.basemvp.main.contracts.HomeContract;
import basemvp.dev.kb.basemvp.main.presenters.HomePresenter;
import basemvp.dev.kb.basemvp.main.adapter.HomeImageListAdapter;
import butterknife.BindView;
import butterknife.OnClick;

public class HomeActivity extends MVPActivity<HomePresenter> implements HomeContract.ViewImpl {
    @BindView(R.id.tvTest)
    TextView tvTest;
    @BindView(R.id.rcvImageList)
    RecyclerView mRecyclerView;

    HomeImageListAdapter mAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.btnChangeBackground)
    void onBtnChangeBackgroundClick() {
        getPresenter().updateBackGround();
    }

    @OnClick(R.id.btnChangeText)
    void onBtnUpdateTextClick() {
        getPresenter().updateText();
    }

    @Override
    public void setText(String text) {
        tvTest.setText(text);
    }

    @Override
    public void setBackGroundColor(int color) {
        getWindow().getDecorView().getRootView().setBackgroundColor(color);
    }

    @Override
    public void fillImage(List<String> data) {
        mAdapter.replace(data);
    }

    @Override
    public void onViewAppear() {
        Toast.makeText(this, "onViewAppear", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewDisappear() {
        Toast.makeText(this, "onViewDisappear", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initView() {
        mAdapter = new HomeImageListAdapter();
        ViewHelper.setupRecycle(mRecyclerView, new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false), mAdapter);
    }

    @OnClick(R.id.btnLoadImage)
    void onBtnLoadImagesBtnClick() {
        getPresenter().loadImages();
    }
}
