package basemvp.dev.kb.basemvp.services;

import basemvp.dev.kb.basemvp.main.models.responses.GetPhotosRSP;
import retrofit2.Call;
import retrofit2.http.GET;

public interface DemoService {
    @GET("images")
    Call<GetPhotosRSP> getImages();
}
