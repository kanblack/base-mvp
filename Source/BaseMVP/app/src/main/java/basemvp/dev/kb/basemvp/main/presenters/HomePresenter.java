package basemvp.dev.kb.basemvp.main.presenters;

import android.graphics.Color;

import com.paditech.base.mvp.BasePresenter;

import java.util.Random;

import basemvp.dev.kb.basemvp.main.contracts.HomeContract;
import basemvp.dev.kb.basemvp.main.models.responses.GetPhotosRSP;
import basemvp.dev.kb.basemvp.services.DemoClient;
import basemvp.dev.kb.basemvp.services.DemoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter extends BasePresenter<HomeContract.ViewImpl> implements HomeContract.Presenter {
    private DemoService service;

    @Override
    public void onCreate() {
        super.onCreate();
        service = DemoClient.newInstance().getService();
    }

    @Override
    public void updateBackGround() {
        Random rand = new Random();
        int r = rand.nextInt(255);
        int g = rand.nextInt(255);
        int b = rand.nextInt(255);
        getView().setBackGroundColor(Color.rgb(r, g, b));
    }

    @Override
    public void updateText() {
        getView().setText(System.currentTimeMillis() + "");
    }

    @Override
    public void loadImages() {
        getView().showProgressDialog(true);
        service.getImages().enqueue(new Callback<GetPhotosRSP>() {
            @Override
            public void onResponse(Call<GetPhotosRSP> call, Response<GetPhotosRSP> response) {
                getView().showProgressDialog(false);
                getView().fillImage(response.body().getData());
            }

            @Override
            public void onFailure(Call<GetPhotosRSP> call, Throwable t) {
                getView().showProgressDialog(false);
            }
        });
    }
}
