package com.paditech.base.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseRecycleViewAdapter extends RecyclerView.Adapter<BaseRecycleViewAdapter.BaseViewHolder> implements BaseConstant {
    public interface LoadMoreListener {
        void onLoadMore();
    }

    public interface ItemClickListener {
        void onItemClick(BaseViewHolder holder, View view, int position);
    }

    private LoadMoreListener onLoadMoreListener;
    private ItemClickListener onItemClickListener;

    public void setOnLoadMoreListener(LoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setOnItemClickListener(ItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bindView(position);
        if (position == getItemCount() - 1 && onLoadMoreListener != null) {
            onLoadMoreListener.onLoadMore();
        }
    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        int index;

        public BaseViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(BaseViewHolder.this, itemView, index);
                    }
                }
            });
        }

        protected void bindView(int position) {
            index = position;
            onBindingData(position);
        }

        protected abstract void onBindingData(int position);
    }


    public abstract Object getItem(int position);
}
