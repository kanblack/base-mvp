package com.paditech.base.image;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

@GlideModule
public final class BaseGlideModule extends AppGlideModule {
    private final static int CacheSize = 250000000;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 5;
        GlideBuilder glideBuilder = new GlideBuilder();
        glideBuilder.setDiskCache(
                new DiskLruCacheFactory(context.getCacheDir().getAbsolutePath(), CacheSize));
        glideBuilder.setMemoryCache(new LruResourceCache(cacheSize));
        glideBuilder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);
    }

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        super.registerComponents(context, glide, registry);
        glide.setMemoryCategory(MemoryCategory.HIGH);
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS);

//        glide.getRegistry().replace(GlideUrl.class, InputStream.class, a);

    }
}
