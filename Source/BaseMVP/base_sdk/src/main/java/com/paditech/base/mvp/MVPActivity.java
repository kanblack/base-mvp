package com.paditech.base.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;

import com.paditech.base.BaseActivity;

public abstract class MVPActivity<T extends BasePresenter> extends BaseActivity implements BaseViewImpl {
    private T mPresenter;

    public T getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            mPresenter = (T) BasePresenter.initialize(getClass());
            Preconditions.checkNotNull(mPresenter).setView(this);
            super.onCreate(savedInstanceState);
            mPresenter.onCreate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewAppear() {
        mPresenter.onViewAppear();
    }

    @Override
    public void onViewDisappear() {
        mPresenter.onViewDisAppear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
