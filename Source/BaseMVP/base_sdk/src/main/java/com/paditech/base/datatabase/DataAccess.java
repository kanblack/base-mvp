package com.paditech.base.datatabase;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DataAccess implements DataAccessImpl {
    private final Realm mRealm;

    private DataAccess() {
        mRealm = getNewRealm();
    }

    private static Realm getNewRealm() {
        return Realm.getInstance(new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build());
    }

    public DataAccessImpl getInstance() {
        return this;
    }
}
