package com.paditech.base.network;

import com.google.common.base.Preconditions;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Should not modify here
 */
class NetworkConfig implements NetworkConfigImpl {
    private OkHttpClient.Builder mBuilder;

    private NetworkConfig() {
        mBuilder = new OkHttpClient.Builder();
    }

    static NetworkConfigImpl newInstance() {
        return new NetworkConfig();
    }

    @Override
    public OkHttpClient.Builder getBuilder() {
        return mBuilder;
    }

    @Override
    public NetworkConfigImpl setupProxy(String proxyHost, int proxyPort) {
        Preconditions.checkNotNull(proxyHost, "proxy host cannot be null");
        java.net.Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        mBuilder.proxy(proxy);
        return this;
    }

    @Override
    public NetworkConfigImpl setReadTimeOut(long timeOut, TimeUnit unit) {
        mBuilder.readTimeout(120, TimeUnit.SECONDS);
        return this;
    }

    @Override
    public NetworkConfigImpl setConnectTimeOut(long timeOut, TimeUnit unit) {
        mBuilder.connectTimeout(120, TimeUnit.SECONDS);
        return this;
    }

    @Override
    public OkHttpClient build() {
        return mBuilder.build();
    }
}
