package com.paditech.base.common;

/**
 * Use to storage all the simple data
 */
public interface BaseConstant {
    String EMPTY = "";

    interface Error
    {
        String NO_NETWORK = "No network";
        String UNKNOWn = "Unknown error";
        String NOT_VALID = "Your input is not valid!";
    }

    interface Message
    {
        String SUCCESS = "Success";
        String FAILURE = "Failure";
        String OK = "Ok";
        String CANCEL = "Cancel";
    }
}
