package com.paditech.base;

/**
 * Should not modify here
 */
public interface ViewStateListener extends ViewCommonAction {
    void initView();

    void onViewAppear();

    void onViewDisappear();
}
