package com.paditech.base.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Preconditions;

import com.paditech.base.BaseFragment;

public abstract class MVPFragment<T extends BasePresenter> extends BaseFragment implements BaseViewImpl {
    private T mPresenter;

    public T getPresenter() {
        return mPresenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            boolean isFirstTime = mPresenter == null;
            if (mPresenter == null) {
                //noinspection unchecked
                mPresenter = (T) BasePresenter.initialize(getClass());
                Preconditions.checkNotNull(mPresenter).setView(this);
            }
            View view = super.onCreateView(inflater, container, savedInstanceState);
            if (isFirstTime) {
                mPresenter.onCreate();
            }
            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewDisappear() {
        super.onViewDisappear();
        mPresenter.onViewDisAppear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.onDestroy();
    }

    @Override
    public void onViewAppear() {
        super.onViewAppear();
        mPresenter.onViewAppear();
    }
    @Override
    public Context getContext() {
        return getActivity();
    }
}
