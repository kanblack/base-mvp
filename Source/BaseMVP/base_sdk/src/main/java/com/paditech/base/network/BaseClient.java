package com.paditech.base.network;

import android.support.annotation.NonNull;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Should not modify here
 */
public abstract class BaseClient<T> {
    private static final int CACHE_SIZE = 10 * 1024 * 1024;// 10 MiB
    private T mService;

    protected abstract String getHostAddress();

    protected abstract Class<T> getServiceType();

    public static <O> O init(@NonNull Class<O> clazz) {
        try {
            Type genericSuperClass = clazz.getGenericSuperclass();
            Type genericType = ((ParameterizedType) genericSuperClass).getActualTypeArguments()[0];
            return (O) Class.forName(genericType.toString().replace("class ", "")).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T getService() {
        if (mService == null) {
            synchronized (BaseClient.class) {
                //Setup base config
                NetworkConfigImpl config = NetworkConfig.newInstance();
                config.setReadTimeOut(120, TimeUnit.SECONDS)
                        .setConnectTimeOut(120, TimeUnit.SECONDS);
                //.setupProxy("", 1);
                onConfigsSetup(config);

                //setup cache
//                File httpCacheDirectory = new File(BaseApplication.getAppContext().getCacheDir(), "responses");
//                Cache cache = new Cache(httpCacheDirectory, CACHE_SIZE);

                //add cache to the client
//                config.getBuilder().cache(cache);

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                config.getBuilder().addInterceptor(logging);

                //add common Interceptor
                config.getBuilder().addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws java.io.IOException {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    }
                });
                //Build
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getHostAddress())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(config.build())
                        .build();

                //Create service
                mService = retrofit.create(getServiceType());
            }
        }
        return mService;
    }

    public NetworkConfigImpl onConfigsSetup(NetworkConfigImpl config) {
        return config;
    }
}
