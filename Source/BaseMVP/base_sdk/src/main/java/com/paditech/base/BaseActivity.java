package com.paditech.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.paditech.base.common.BaseConstant;
import com.paditech.base.helper.ViewHelper;

import butterknife.ButterKnife;

/**
 * Should not modify here
 */
public abstract class BaseActivity extends AppCompatActivity implements ViewStateListener, ViewCommonAction, BaseConstant {
    private ViewGroup layoutRoot;
    private View layoutLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_default);
        layoutRoot = findViewById(R.id.root);
        layoutLoading = findViewById(R.id.layoutLoading);
        View.inflate(this, getLayoutResource(), (ViewGroup) findViewById(R.id.root));
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        onViewAppear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        onViewDisappear();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    protected abstract int getLayoutResource();

    @Override
    public void initView() {

    }

    @Override
    public void onViewAppear() {
        try {
            View content = this.findViewById(android.R.id.content).getRootView();
            ViewHelper.hideKeyboardOntTouchOutside(this, content);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onViewDisappear() {
        ViewHelper.hideSoftKeyboard(this);
    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void showAlertDialog(String string, String s, View.OnClickListener onActionListener) {

    }

    @Override
    public void showAlertDialog(String title, String message, View.OnClickListener onActionListener, DialogInterface.OnCancelListener onCancelListener) {

    }

    @Override
    public void showConfirmDialog() {

    }

    @Override
    public void showProgressDialog(boolean isShown) {
        layoutLoading.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEnableTouch(boolean enableTouch) {

    }

    @Override
    public void log(int debugType, String message) {

    }

    @Override
    public void switchFragment(Fragment target, boolean isAddToBackStack) {

    }

    @Override
    public void onReceiveIntent(Context context, Intent intent) {

    }

    @Override
    public void sendIntent(Intent intent) {

    }

    @Override
    public Context getContext() {
        return this;
    }
}