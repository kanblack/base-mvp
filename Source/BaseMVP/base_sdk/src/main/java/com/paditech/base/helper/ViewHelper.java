package com.paditech.base.helper;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.paditech.base.common.BaseConstant;

public class ViewHelper {
    public static void setupRecycle(@NonNull RecyclerView rcv, @NonNull RecyclerView.LayoutManager layoutManager, @NonNull RecyclerView.Adapter adapter) {
        if(rcv == null)
        {
            return;
        }
        rcv.setLayoutManager(layoutManager);
        rcv.setAdapter(adapter);
    }

    public static void setText(TextView textView, String text, String defaultText) {
        if (textView == null) {
            return;
        }
        if (defaultText == null) {
            defaultText = BaseConstant.EMPTY;
        }
        if (text == null || text.isEmpty()) {
            text = defaultText;
        }
        textView.setText(text);
    }

    public static void hideKeyboardOntTouchOutside(final Activity activity, View view)
    {
        if (activity == null || view == null) {
            return;
        }

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboardOntTouchOutside(activity, innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            View focusView = activity.getCurrentFocus();
            if (focusView != null) {
                inputMethodManager.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
                focusView.clearFocus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
