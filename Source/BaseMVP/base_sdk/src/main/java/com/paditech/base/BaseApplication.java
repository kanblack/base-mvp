package com.paditech.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import java.lang.ref.WeakReference;

import io.realm.Realm;


public class BaseApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private static WeakReference<Context> context;

    public static Context getAppContext() {
        return context.get();
    }

    public void onCreate() {
        super.onCreate();
        context = new WeakReference<Context>(this);
        registerActivityLifecycleCallbacks(this);
        Realm.init(this);
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
