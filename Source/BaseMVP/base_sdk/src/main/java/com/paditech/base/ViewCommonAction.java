package com.paditech.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

public interface ViewCommonAction {
    void showToast(String message);

    void showAlertDialog(String string, String s, View.OnClickListener onActionListener);

    void showAlertDialog(String title, String message, View.OnClickListener onActionListener, DialogInterface.OnCancelListener onCancelListener);

    void showConfirmDialog();

    void showProgressDialog(boolean isShown);

    void setEnableTouch(boolean enableTouch);

    void log(int debugType, String message);

    void switchFragment(Fragment target, boolean isAddToBackStack);

    void onReceiveIntent(Context context, Intent intent);

    void sendIntent(Intent intent);

    Context getContext();
}