package com.paditech.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paditech.base.common.BaseConstant;
import butterknife.ButterKnife;

/**
 * Should not modify here
 */
public abstract class BaseFragment extends Fragment implements ViewStateListener, ViewCommonAction, BaseConstant {
    private View lastView;
    private boolean isFirstTime = true;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            View view = lastView;
            if (isKeepFragment()) {
                if (lastView == null) {
                    view = lastView = inflater.inflate(getLayoutResource(), null, false);
                }
            } else {
                view = inflater.inflate(getLayoutResource(), null, false);
            }
            ButterKnife.bind(this, view);

            if (isFirstTime) {
                initView();
            }
            isFirstTime = false;
            onViewAppear();
            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        onViewAppear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onViewDisappear();
    }

    protected abstract int getLayoutResource();

    protected boolean isKeepFragment() {
        return false;
    }

    @Override
    public void initView() {

    }

    @Override
    public void onViewAppear() {
    }

    @Override
    public void onViewDisappear() {

    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void showAlertDialog(String string, String s, View.OnClickListener onActionListener) {

    }

    @Override
    public void showAlertDialog(String title, String message, View.OnClickListener onActionListener, DialogInterface.OnCancelListener onCancelListener) {

    }

    @Override
    public void showConfirmDialog() {

    }

    @Override
    public void showProgressDialog(boolean isShown) {

    }

    @Override
    public void setEnableTouch(boolean enableTouch) {

    }

    @Override
    public void log(int debugType, String message) {

    }

    @Override
    public void switchFragment(Fragment target, boolean isAddToBackStack) {

    }

    @Override
    public void onReceiveIntent(Context context, Intent intent) {

    }

    @Override
    public void sendIntent(Intent intent) {

    }
}
