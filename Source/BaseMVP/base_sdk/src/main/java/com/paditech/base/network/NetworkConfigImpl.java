package com.paditech.base.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Should not modify here
 */
interface NetworkConfigImpl {
    NetworkConfigImpl setupProxy(String proxyHost, int proxyPort);

    NetworkConfigImpl setReadTimeOut(long timeOut, TimeUnit unit);

    NetworkConfigImpl setConnectTimeOut(long timeOut, TimeUnit unit);

    OkHttpClient.Builder getBuilder();

    OkHttpClient build();
}
