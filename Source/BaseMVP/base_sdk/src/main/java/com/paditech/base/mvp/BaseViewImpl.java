package com.paditech.base.mvp;

import android.content.Context;

import com.paditech.base.ViewCommonAction;

public interface BaseViewImpl extends ViewCommonAction {
    Context getContext();
}
